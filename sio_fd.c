/*
**  OSSP sio - Stream I/O
**  Copyright (c) 2002-2005 Cable & Wireless <http://www.cw.com/>
**  Copyright (c) 2002-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2005 Ralf S. Engelschall <rse@engelschall.com>
**
**  This file is part of OSSP sio, a layered stream I/O library
**  which can be found at http://www.ossp.org/pkg/lib/sio/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  sio_fd.h: filedescriptor stage
*/

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

#include "al.h"
#include "sio.h"

typedef struct {
    char *mem;
    size_t size;
} buffer_t;

typedef struct {
    int fd;
    buffer_t input;
    size_t written;
    al_label_t data_label;
    al_label_t error_label;
    al_label_t eof_label;
    char eof, error;
} private_t;

/*
 * create stage
 *
 * allocate private instance data
 */
static
sio_rc_t fd_init(sio_t *sio, void **up)
{
    private_t *my;

    my = (private_t *)malloc(sizeof(private_t));
    if (my == NULL)
        return SIO_ERR_MEM;

    my->fd           = -1;

    my->input.mem    = NULL;
    my->input.size   = 0;

    sio_label(sio, SIO_LN_DATA,  &my->data_label);
    sio_label(sio, SIO_LN_ERROR, &my->error_label);
    sio_label(sio, SIO_LN_EOF,   &my->eof_label);

    my->eof          = '\0';
    my->error        = '\0';

    *up = my;

    return SIO_OK;
}

/*
 * configure stage
 *
 */
static
sio_rc_t fd_configure(sio_t *sio, void *u, void *obj, void *val)
{
    private_t *my = (private_t *)u;
    const char *name = (const char *)obj;

    if (!strcmp(name, "fd")) {
        my->fd = *(int *)val;
    } else if (!strcmp(name, "buflen")) {
        my->input.size = *(size_t *)val;
    } else {
        return SIO_ERR_ARG;
    }

    return SIO_OK;
}

/*
 * destroy stage
 */
static
sio_rc_t fd_cleanup(sio_t *sio, void *u)
{
    private_t *my = (private_t *)u;

    free(my);

    return SIO_OK;
}

static
sio_rc_t fd_openr(sio_t *sio, al_t *al, void *u)
{
    private_t *my = (private_t *)u;
    buffer_t *buf = &my->input;
    char *p;
    size_t n;

    n = buf->size;
    if (n == 0)
        return SIO_ERR_ARG;

    p = realloc(buf->mem, n);
    if (p != NULL)
        buf->mem = p;

    if (buf->mem == NULL)
        return SIO_ERR_MEM;

    return SIO_OK;
}

static
sio_rc_t fd_closer(sio_t *sio, al_t *al, void *u)
{
    private_t *my = (private_t *)u;
    buffer_t *buf = &my->input;

    if (buf->mem != NULL) {
        free(buf->mem);
        buf->mem = NULL;
    }

    return SIO_OK;
}

static
sio_rc_t fd_openw(sio_t *sio, al_t *al, void *u)
{
    return SIO_OK;
}

static
sio_rc_t fd_closew(sio_t *sio, al_t *al, void *u)
{
    return SIO_OK;
}

static
sio_rc_t fd_input(sio_t *sio, al_t *al, void *u, sio_rc_t orc)
{
    private_t *my = (private_t *)u;
    buffer_t *buf = &my->input;
    int actual;

    actual = read(my->fd, buf->mem, buf->size);
    if (actual < 0) {
        al_append_bytes(al, &my->error, sizeof(my->error), my->error_label);
        return SIO_SCHED_DOWN;
    } else if (actual == 0) {
        al_append_bytes(al, &my->eof, sizeof(my->eof), my->eof_label);
        return SIO_SCHED_DOWN;
    }

    al_append_bytes(al, buf->mem, actual, my->data_label);

    return SIO_SCHED_DOWN;
}

static
al_rc_t fd_output_chunk(al_chunk_t *alc, void *u)
{
    private_t *my = (private_t *)u;
    char   *p;
    size_t n;
    int actual;

    p = al_chunk_ptr(alc, 0);
    n = al_chunk_len(alc);

    actual = write(my->fd, p, n);
    if (actual < 0)
        return AL_ERR_EOF;

    my->written += actual;

    return AL_OK;
}

static
sio_rc_t fd_output(sio_t *sio, al_t *al, void *u, sio_rc_t orc)
{
    private_t *my = (private_t *)u;
    al_rc_t arc;
    size_t n = al_bytes(al);

    my->written = 0;

    arc = al_traverse_cb(al, 0, n, AL_FORWARD, my->data_label,
                         fd_output_chunk, u);
    if (arc != AL_OK)
        return SIO_SCHED_DOWN;

    arc = al_splice(al, 0, al_bytes(al), NULL, NULL);
    if (arc != AL_OK)
        return SIO_SCHED_DOWN;

    return SIO_SCHED_DOWN;
}

sio_module_t sio_module_fd = {
    "fd",
    fd_init,
    fd_configure,
    fd_cleanup,
    fd_openr,
    fd_closer,
    fd_openw,
    fd_closew,
    fd_input,
    fd_output,
    NULL
};


