


substdio(3)                                           substdio(3)


NNAAMMEE
       substdio - the Sub-Standard Input/Output Library

SSYYNNTTAAXX
       ##iinncclluuddee <<ssuubbssttddiioo..hh>>

       void ssuubbssttddiioo__ffddbbuuff(&_s,_o_p,_f_d,_b_u_f,_l_e_n);

       int ssuubbssttddiioo__ffiilleennoo(&_s);

       substdio _s;
       int (*_o_p)();
       int _f_d;
       char *_b_u_f;
       int _l_e_n;

DDEESSCCRRIIPPTTIIOONN
       ssuubbssttddiioo  is  the Sub-Standard I/O Library.  ssuubbssttddiioo con-
       tains only a few of the features of stdio; it is  a  fast,
       lightweight, low-level library, suitable for use as a com-
       ponent of higher-level I/O libraries.

       The point of ssuubbssttddiioo is to  provide  buffered  I/O.   The
       basic object in ssuubbssttddiioo is the ssuubbssttddiioo structure; a ssuubb--
       ssttddiioo variable stores an operation, a  descriptor,  and  a
       pointer  into a buffer of some nonzero length.  The ssuubbsstt--
       ddiioo operations read data  from  the  buffer,  filling  the
       buffer as necessary using the operation on the descriptor,
       or write data to the buffer, flushing the buffer as neces-
       sary  using  the  operation  on the descriptor.  Input and
       output operations cannot be mixed.

       ssuubbssttddiioo__ffddbbuuff initializes a ssuubbssttddiioo variable.  The oper-
       ation is _o_p.  The descriptor is _f_d.  The buffer is _b_u_f, an
       array of _l_e_n chars.

       _o_p will be called as _o_p(_f_d,_x,_n).  Here _x is a  pointer  to
       an  array  of  characters  of  length _n; _o_p must read some
       characters from _f_d to that array, or write some characters
       to  _f_d  from that array.  The return value from _o_p must be
       the number of characters read or  written.   0  characters
       read  means  end of input; 0 characters written means that
       the write operation should be tried again immediately.  On
       error,  _o_p  must  return  -1, setting eerrrrnnoo appropriately,
       without reading or  writing  anything.   Most  errors  are
       returned  directly to the ssuubbssttddiioo caller, but an error of
       eerrrroorr__iinnttrr means that the operation should be tried  again
       immediately.

       There is a SSUUBBSSTTDDIIOO__FFDDBBUUFF macro that can be used to stati-
       cally initialize a ssuubbssttddiioo variable:

          substdio s = SUBSTDIO_FDBUF(op,fd,buf,len);




                                                                1





substdio(3)                                           substdio(3)


       ssuubbssttddiioo__ffiilleennoo returns the descriptor for an  initialized
       ssuubbssttddiioo variable.

SSEEEE AALLSSOO
       substdio_in(3),     substdio_out(3),     substdio_copy(3),
       error(3)



















































                                                                2





substdio_in(3)                                     substdio_in(3)


NNAAMMEE
       substdio_in - substdio input routines

SSYYNNTTAAXX
       ##iinncclluuddee <<ssuubbssttddiioo..hh>>

       int ssuubbssttddiioo__ggeett(&_s,_t_o,_l_e_n);

       int ssuubbssttddiioo__bbggeett(&_s,_t_o,_l_e_n);

       int ssuubbssttddiioo__ffeeeedd(&_s);

       char *ssuubbssttddiioo__ppeeeekk(&_s);

       void ssuubbssttddiioo__sseeeekk(&_s,_l_e_n);

       substdio _s;
       char *_t_o;
       int _l_e_n;

DDEESSCCRRIIPPTTIIOONN
       ssuubbssttddiioo__ggeett  reads at most _l_e_n characters from _s into the
       character array _t_o.  It returns the number  of  characters
       read,  0  for  end of file, or -1 for error, setting eerrrrnnoo
       appropriately.

       ssuubbssttddiioo__bbggeett has the same function as ssuubbssttddiioo__ggeett.   The
       difference  is what happens when there is no buffered data
       and _l_e_n exceeds the buffer  size:  ssuubbssttddiioo__ggeett  tries  to
       read  _l_e_n  characters, whereas ssuubbssttddiioo__bbggeett tries to read
       one buffer of characters.   In  some  cases  ssuubbssttddiioo__bbggeett
       will be more efficient than ssuubbssttddiioo__ggeett.

       ssuubbssttddiioo__ffeeeedd  makes  sure that there is buffered data, so
       that the next ssuubbssttddiioo__ggeett or ssuubbssttddiioo__bbggeett will  succeed.
       If the buffer is empty, ssuubbssttddiioo__ffeeeedd tries to fill it; it
       returns 0 for end of file, -1 for error, or the number  of
       buffered characters on success.  If the buffer already had
       data, ssuubbssttddiioo__ffeeeedd leaves it alone and returns the number
       of buffered characters.

       ssuubbssttddiioo__ppeeeekk returns a pointer to the buffered data.

       ssuubbssttddiioo__sseeeekk  throws  away _l_e_n buffered characters, as if
       they had been read.  _l_e_n must be at least 0  and  at  most
       the amount of buffered data.

       The ssuubbssttddiioo__PPEEEEKK and ssuubbssttddiioo__SSEEEEKK macros behave the same
       way as ssuubbssttddiioo__ppeeeekk and ssuubbssttddiioo__sseeeekk  but  may  evaluate
       their arguments several times.

       The  point  of  ssuubbssttddiioo__ppeeeekk and ssuubbssttddiioo__sseeeekk is to read
       data without unnecessary copies.  Sample code:




                                                                1





substdio_in(3)                                     substdio_in(3)


         for (;;) {
           n = substdio_feed(s);
           if (n <= 0) return n;
           x = substdio_PEEK(s);
           handle(x,n);
           substdio_SEEK(s,n);
         }

       The SSUUBBSSTTDDIIOO__IINNSSIIZZEE macro is defined as a reasonably large
       input buffer size for ssuubbssttddiioo__ffddbbuuff.

IINNTTEERRNNAALLSS
       When  a  ssuubbssttddiioo  variable  _s is used for input, there is
       free buffer space from _s..xx to _s..xx + _s..nn; data is  buffered
       from _s..xx + _s..nn to _s..xx + _s..nn + _s..pp; the total buffer length
       is _s..nn + _s..pp.

SSEEEE AALLSSOO
       substdio(3)






































                                                                2





substdio_out(3)                                   substdio_out(3)


NNAAMMEE
       substdio_out - substdio output routines

SSYYNNTTAAXX
       ##iinncclluuddee <<ssuubbssttddiioo..hh>>

       int ssuubbssttddiioo__ppuutt(&_s,_f_r_o_m,_l_e_n);
       int ssuubbssttddiioo__ppuuttss(&_s,_f_r_o_m);

       int ssuubbssttddiioo__bbppuutt(&_s,_f_r_o_m,_l_e_n);
       int ssuubbssttddiioo__bbppuuttss(&_s,_f_r_o_m);

       int ssuubbssttddiioo__fflluusshh(&_s);

       int ssuubbssttddiioo__ppuuttfflluusshh(&_s,_f_r_o_m,_l_e_n);
       int ssuubbssttddiioo__ppuuttssfflluusshh(&_s,_f_r_o_m);

       substdio _s;
       char *_f_r_o_m;
       int _l_e_n;

DDEESSCCRRIIPPTTIIOONN
       ssuubbssttddiioo__ppuutt writes _l_e_n characters to _s out of the charac-
       ter array _f_r_o_m.  It returns 0 on success, -1 on error.

       ssuubbssttddiioo__bbppuutt has the same function as ssuubbssttddiioo__ppuutt.   The
       difference  is  how the buffer is flushed when there isn't
       enough room for _l_e_n characters: ssuubbssttddiioo__ppuutt  flushes  the
       buffered  data before copying the new data, whereas ssuubbsstt--
       ddiioo__bbppuutt fills the buffer with new data before flushing.

       ssuubbssttddiioo__fflluusshh forces all data  to  be  written  from  the
       internal buffer.  It returns 0 on success, -1 on error.

       ssuubbssttddiioo__ppuuttfflluusshh  is  similar to ssuubbssttddiioo__ppuutt followed by
       ssuubbssttddiioo__fflluusshh, but it avoids all internal copies.

       ssuubbssttddiioo__ppuuttss, ssuubbssttddiioo__bbppuuttss, and ssuubbssttddiioo__ppuuttssfflluusshh  are
       the same as ssuubbssttddiioo__ppuutt, ssuubbssttddiioo__bbppuutt, and ssuubbssttddiioo__ppuutt--
       fflluusshh except that _f_r_o_m must be a  0-terminated  string  of
       characters.   The string, not including the 0, is written.

       The SSUUBBSSTTDDIIOO__OOUUTTSSIIZZEE macro  is  defined  as  a  reasonably
       large output buffer size for ssuubbssttddiioo__ffddbbuuff.

IINNTTEERRNNAALLSS
       When  a  ssuubbssttddiioo  variable  _s is used for output, data is
       buffered from _s..xx to _s..xx + _s..pp; there is free buffer space
       from to _s..xx + _s..pp to _s..xx + _s..nn; the total buffer length is
       _s..nn.

SSEEEE AALLSSOO
       substdio(3)




                                                                1





substdio_copy(3)                                 substdio_copy(3)


NNAAMMEE
       substdio_copy - copy an entire input to an output

SSYYNNTTAAXX
       ##iinncclluuddee <<ssuubbssttddiioo..hh>>

       int ssuubbssttddiioo__ccooppyy(&_s_o_u_t,&_s_i_n);

       substdio _s_o_u_t;
       substdio _s_i_n;

DDEESSCCRRIIPPTTIIOONN
       ssuubbssttddiioo__ccooppyy reads characters from _s_i_n until end of file,
       writing each character to _s_o_u_t.  It then  returns  0.   It
       does not flush _s_o_u_t.

       Upon  a _s_i_n error, ssuubbssttddiioo__ccooppyy returns -2, leaving eerrrrnnoo
       set appropriately.

       Upon a _s_o_u_t error, ssuubbssttddiioo__ccooppyy returns -3, leaving eerrrrnnoo
       set appropriately.

SSEEEE AALLSSOO
       substdio(3)

































                                                                1


