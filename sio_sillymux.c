/*
**  OSSP sio - Stream I/O
**  Copyright (c) 2002-2005 Cable & Wireless <http://www.cw.com/>
**  Copyright (c) 2002-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2005 Ralf S. Engelschall <rse@engelschall.com>
**
**  This file is part of OSSP sio, a layered stream I/O library
**  which can be found at http://www.ossp.org/pkg/lib/sio/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  sio_sillymux.c: silly multiplexing stage
*/

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "al.h"
#include "sio.h"

typedef struct {
    al_label_t odd_label;
    al_label_t even_label;
    al_label_t data_label;
    int        odd;
    size_t     next;
    int        found;
} private_t;

/*
 * create stage
 *
 * allocate private instance data
 */
static
sio_rc_t sillymux_init(sio_t *sio, void **up)
{
    private_t *my;

    my = (private_t *)malloc(sizeof(private_t));
    if (my == NULL)
        return SIO_ERR_MEM;

    my->odd_label    = NULL;
    my->even_label   = NULL;

    my->found        = 0;
    my->odd          = 1;

    sio_label(sio, SIO_LN_DATA, &my->data_label);

    *up = my;

    return SIO_OK;
}

/*
 * configure stage
 *
 * pass two void pointers
 */
static
sio_rc_t sillymux_configure(sio_t *sio, void *u, void *obj, void *val)
{
    private_t *my = (private_t *)u;
    const char *name = (const char *)obj;

    if (!strcmp(name, "evenlabel")) {
        my->even_label = (al_label_t)val;
    } else if (!strcmp(name, "oddlabel")) {
        my->odd_label = (al_label_t)val;
    } else {
        return SIO_ERR_ARG;
    }

    return SIO_OK;
}

/*
 * destroy stage
 */
static
sio_rc_t sillymux_cleanup(sio_t *sio, void *u)
{
    private_t *my = (private_t *)u;

    free(my);

    return SIO_OK;
}

static
sio_rc_t sillymux_openr(sio_t *sio, al_t *al, void *u)
{
    return SIO_OK;
}

static
sio_rc_t sillymux_closer(sio_t *sio, al_t *al, void *u)
{
    return SIO_OK;
}

static
sio_rc_t sillymux_openw(sio_t *sio, al_t *al, void *u)
{
    return SIO_OK;
}

static
sio_rc_t sillymux_closew(sio_t *sio, al_t *al, void *u)
{
    return SIO_OK;
}

static
al_rc_t findlf(al_chunk_t *alc, void *u)
{
    private_t *my = (private_t *)u;
    const char *p, *lf;
    size_t n;

    n = al_chunk_len(alc);

    if (!al_same_label(alc, my->data_label)) {
        my->next += n;
        return AL_ERR_EOF;
    }

    p = al_chunk_ptr(alc,0);
    lf = memchr(p, '\n', n);
    if (lf) {
        my->next += lf - p + 1;
        my->found = 1;
        return AL_ERR_EOF;
    }

    my->next += n;

    return AL_OK;
}

static
sio_rc_t sillymux_input(sio_t *sio, al_t *al, void *u, sio_rc_t orc)
{
    private_t *my = (private_t *)u;
    size_t pos, n;

    n = al_bytes(al);
    if (n == 0)
        return SIO_SCHED_UP;

    for (pos=0; pos<n; pos = my->next) {

        my->next = pos;
        al_traverse_cb(al, pos, n-pos, AL_FORWARD, NULL, findlf, u);

        al_setlabel(al, pos, my->next - pos,
                    my->data_label,
                    my->odd ? my->odd_label : my->even_label);

        if (my->found) {
            my->found = 0;
            my->odd = !my->odd;
        }
    }

    return SIO_SCHED_DOWN;
}

static
sio_rc_t sillymux_output(sio_t *sio, al_t *al, void *u, sio_rc_t orc)
{
    private_t *my = (private_t *)u;

    if (al_bytes(al) == 0)
        return SIO_SCHED_DOWN;

    al_setlabel(al, 0, al_bytes(al), my->odd_label, my->data_label);
    al_setlabel(al, 0, al_bytes(al), my->even_label, my->data_label);

    return SIO_SCHED_UP;
}

sio_module_t sio_module_sillymux = {
    "sillymux",
    sillymux_init,
    sillymux_configure,
    sillymux_cleanup,
    sillymux_openr,
    sillymux_closer,
    sillymux_openw,
    sillymux_closew,
    sillymux_input,
    sillymux_output,
    NULL
};

