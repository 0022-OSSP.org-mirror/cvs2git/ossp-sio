/*
**  OSSP sio - Stream I/O
**  Copyright (c) 2002-2005 Cable & Wireless <http://www.cw.com/>
**  Copyright (c) 2002-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2005 Ralf S. Engelschall <rse@engelschall.com>
**
**  This file is part of OSSP sio, a layered stream I/O library
**  which can be found at http://www.ossp.org/pkg/lib/sio/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  sio_hole.c: black hole stage
*/

#include <stddef.h>
#include <stdlib.h>

#include "al.h"
#include "sio.h"

typedef struct {
    int dummy;
} private_t;

/*
 * create stage
 *
 * allocate private instance data
 */
static
sio_rc_t hole_init(sio_t *sio, void **up)
{
    private_t *my;

    my = (private_t *)malloc(sizeof(private_t));
    if (my == NULL)
        return SIO_ERR_MEM;

    *up = my;

    return SIO_OK;
}

/*
 * configure stage
 *
 * pass two void pointers
 */
static
sio_rc_t hole_configure(sio_t *sio, void *u, void *obj, void *val)
{
    return SIO_ERR_ARG;
}

/*
 * destroy stage
 */
static
sio_rc_t hole_cleanup(sio_t *sio, void *u)
{
    private_t *my = (private_t *)u;

    free(my);

    return SIO_OK;
}

static
sio_rc_t hole_openr(sio_t *sio, al_t *al, void *u)
{
    return SIO_OK;
}

static
sio_rc_t hole_closer(sio_t *sio, al_t *al, void *u)
{
    return SIO_OK;
}

static
sio_rc_t hole_openw(sio_t *sio, al_t *al, void *u)
{
    return SIO_OK;
}

static
sio_rc_t hole_closew(sio_t *sio, al_t *al, void *u)
{
    return SIO_OK;
}

static
sio_rc_t hole_input(sio_t *sio, al_t *al, void *u, sio_rc_t orc)
{
    /* drop all data into the bit bucket */
    al_splice(al, 0, al_bytes(al), NULL, NULL);

    return SIO_SCHED_DOWN;
}

static
sio_rc_t hole_output(sio_t *sio, al_t *al, void *u, sio_rc_t orc)
{
    /* drop all data into the bit bucket */
    al_splice(al, 0, al_bytes(al), NULL, NULL);

    return SIO_SCHED_DOWN;
}

sio_module_t sio_module_hole = {
    "hole",
    hole_init,
    hole_configure,
    hole_cleanup,
    hole_openr,
    hole_closer,
    hole_openw,
    hole_closew,
    hole_input,
    hole_output,
    NULL
};

