   _        ___  ____ ____  ____        _
  |_|_ _   / _ \/ ___/ ___||  _ \   ___(_) ___
  _|_||_| | | | \___ \___ \| |_) | / __| |/ _ \
 |_||_|_| | |_| |___) |__) |  __/  \__ \ | (_) |
  |_|_|_|  \___/|____/____/|_|     |___/_|\___/

  OSSP sio - Stream I/O
  Version 0.9.3 (03-Oct-2005)

  ABSTRACT

  OSSP sio is an I/O abstraction library for layered stream communication.
  It was built to deal efficiently with complex I/O protocols and includes
  capabilities to filter and multiplex data streams. Its modular structure
  is fully supported by the underlying OSSP al data buffer library.

  COPYRIGHT AND LICENSE

  Copyright (c) 2002-2005 Cable & Wireless <http://www.cw.com/>
  Copyright (c) 2002-2005 The OSSP Project <http://www.ossp.org/>
  Copyright (c) 2002-2005 Ralf S. Engelschall <rse@engelschall.com>

  This file is part of OSSP sio, a layered stream I/O library
  which can be found at http://www.ossp.org/pkg/lib/sio/.

  Permission to use, copy, modify, and distribute this software for
  any purpose with or without fee is hereby granted, provided that
  the above copyright notice and this permission notice appear in all
  copies.

  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.

  HOME AND DOCUMENTATION

  The documentation and latest release can be found on

  o http://www.ossp.org/pkg/lib/sio/
  o  ftp://ftp.ossp.org/pkg/lib/sio/

