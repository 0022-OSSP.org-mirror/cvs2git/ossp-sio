/*
**  OSSP sio - Stream I/O
**  Copyright (c) 2002-2005 Cable & Wireless <http://www.cw.com/>
**  Copyright (c) 2002-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2005 Ralf S. Engelschall <rse@engelschall.com>
**
**  This file is part of OSSP sio, a layered stream I/O library
**  which can be found at http://www.ossp.org/pkg/lib/sio/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  sio.h: stream I/O public API definition
*/

#ifndef __SIO_H__
#define __SIO_H__

typedef enum {
    SIO_OK,
    SIO_TRUE  = SIO_OK,
    SIO_FALSE,
    SIO_ERR_ARG,
    SIO_ERR_MEM,
    SIO_ERR_EOF,
    SIO_ERR_SYS,
    SIO_ERR_INT,
    SIO_SCHED_UP,
    SIO_SCHED_DOWN,
    SIO_SCHED_CROSS,
    SIO_SCHED_LOOP
} sio_rc_t;

typedef enum {
    SIO_MODE_INVALID,
    SIO_MODE_READ,
    SIO_MODE_WRITE,
    SIO_MODE_READWRITE
} sio_mode_t;

typedef enum {
    SIO_FLAG_ERROR,
    SIO_FLAG_EOF
} sio_flag_t;

struct sio_st;
typedef struct sio_st sio_t;

struct sio_stage_st;
typedef struct sio_stage_st sio_stage_t;

struct sio_module_st;
typedef struct sio_module_st sio_module_t;

sio_rc_t    sio_create         (sio_t **siop);
sio_rc_t    sio_destroy        (sio_t  *sio);

sio_rc_t    sio_create_stage   (sio_t  *sio, sio_module_t *siom, sio_stage_t **siosp);
sio_rc_t    sio_destroy_stage  (sio_t  *sio, sio_stage_t *sios);
sio_rc_t    sio_configure_stage(sio_t  *sio, sio_stage_t *sios, void *o, void *v);

sio_rc_t    sio_attach         (sio_t  *sio, sio_stage_t *sios, sio_mode_t rw);
sio_rc_t    sio_detach         (sio_t  *sio, sio_stage_t *sios);

sio_rc_t    sio_input          (sio_t  *sio, al_t *al, size_t limit, al_label_t label);
sio_rc_t    sio_output         (sio_t  *sio, al_t *al);

sio_rc_t    sio_read           (sio_t  *sio, char *dst, size_t n, size_t *actualp);
sio_rc_t    sio_write          (sio_t  *sio, char *src, size_t n, size_t *actualp);

sio_rc_t    sio_push           (sio_t  *sio);

sio_rc_t    sio_flag           (sio_t  *sio, sio_flag_t fl);
sio_rc_t    sio_clearflag      (sio_t  *sio, sio_flag_t fl);

const char *sio_error          (sio_rc_t rc);

/*
 * data internal to modules that should not be exposed to
 * applications
 */

struct sio_module_st {
    const char *name;
    sio_rc_t (*init)      (sio_t *, void **);
    sio_rc_t (*configure) (sio_t *, void *, void *, void *);
    sio_rc_t (*cleanup)   (sio_t *, void *);
    sio_rc_t (*openr)     (sio_t *, al_t *, void *);
    sio_rc_t (*closer)    (sio_t *, al_t *, void *);
    sio_rc_t (*openw)     (sio_t *, al_t *, void *);
    sio_rc_t (*closew)    (sio_t *, al_t *, void *);
    sio_rc_t (*input)     (sio_t *, al_t *, void *, sio_rc_t orc);
    sio_rc_t (*output)    (sio_t *, al_t *, void *, sio_rc_t orc);
    sio_rc_t (*shutdown)  (sio_t *, void *);
};

typedef enum {
    SIO_LN_DATA,
    SIO_LN_ERROR,
    SIO_LN_EOF
} sio_labelnum_t;

sio_rc_t sio_label(sio_t *, sio_labelnum_t, al_label_t *);

#endif /* __SIO_H__ */
