/*
**  OSSP sio - Stream I/O
**  Copyright (c) 2002-2005 Cable & Wireless <http://www.cw.com/>
**  Copyright (c) 2002-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2005 Ralf S. Engelschall <rse@engelschall.com>
**
**  This file is part of OSSP sio, a layered stream I/O library
**  which can be found at http://www.ossp.org/pkg/lib/sio/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  sio_null.c: /dev/null stage
*/

#include <stddef.h>
#include <stdlib.h>

#include "al.h"
#include "sio.h"

typedef struct {
    int a;
    float b;
    char *c;
} private_t;

/*
 * create stage
 *
 * allocate private instance data
 */
static
sio_rc_t null_init(sio_t *sio, void **up)
{
    private_t *my;

    my = (private_t *)malloc(sizeof(private_t));
    if (my == NULL)
        return SIO_ERR_MEM;

    my->a = 42;
    my->b = 42.0;
    my->c = "42";

    *up = my;

    return SIO_OK;
}

/*
 * configure stage
 *
 * pass two void pointers
 */
static
sio_rc_t null_configure(sio_t *sio, void *u, void *obj, void *val)
{
    private_t *my = (private_t *)u;

    my->a = *(int *)val;

    return SIO_OK;
}

/*
 * destroy stage
 */
static
sio_rc_t null_cleanup(sio_t *sio, void *u)
{
    private_t *my = (private_t *)u;

    free(my);

    return SIO_OK;
}

static
sio_rc_t null_openr(sio_t *sio, al_t *al, void *u)
{
    return SIO_OK;
}

static
sio_rc_t null_closer(sio_t *sio, al_t *al, void *u)
{
    return SIO_OK;
}

static
sio_rc_t null_openw(sio_t *sio, al_t *al, void *u)
{
    return SIO_OK;
}

static
sio_rc_t null_closew(sio_t *sio, al_t *al, void *u)
{
    return SIO_OK;
}

static
sio_rc_t null_input(sio_t *sio, al_t *al, void *u, sio_rc_t orc)
{
    return SIO_OK;
}

static
sio_rc_t null_output(sio_t *sio, al_t *al, void *u, sio_rc_t orc)
{
    return SIO_OK;
}

static
sio_rc_t null_shutdown(sio_t *sio, void *u)
{
    return SIO_OK;
}

sio_module_t sio_module_null = {
    "null",
    null_init,
    null_configure,
    null_cleanup,
    null_openr,
    null_closer,
    null_openw,
    null_closew,
    null_input,
    null_output,
    null_shutdown
};

